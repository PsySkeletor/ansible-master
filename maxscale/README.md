Role Name
=========


This Role is for the deployment of the MariaDB Maxscale Database Proxy
Requirements
------------

Ansible 2.1
Maxscale 2.1.x
Keepalived
Maxscale_Exporter

Role Variables
--------------
It is all explained on the vars/main.yml file

Dependencies
------------

License
-------

BSD

Author Information
------------------

Hi! did you like it? or you hate me for being lazy with the doc? i am sorry, but take a look at my blog (in Spanish ) and bully me there :)
https://run.levelcin.co

Nicolas Tobias
